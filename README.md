<p align="center"><img src="http://jarkons.co.id/assets/frontend/images/jarkons/logo/logo_big.png" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
</p>

## About Jarkons



## Installasi 

- Windows
- ```Install Laragon```
- ```Install GIT```
- ```Install NPM```
- ```Install Composer```

## Step Development

- Lakukan Clone dari git dengan perintah
<br>
`git clone http://gitlab.jarkons.com/jarkons/jarkons-api.git *`

- Lakukan Installasi Packages
<br>
`composer install`

- Lakukan Installasi NPM
<br>
`npm install` atau `yarn install`

- Compile JS
<br>
`npm run dev`


