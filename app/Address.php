<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Indonesia;

class Address extends Model
{
    protected $table = "address_user";

    protected $fillable = [
        "alamat",
        "id_user",
        "provinsi_id",
        "kota_id",
        "kecamatan_id",
        "desa_id",
        "rt",
        "rw",
        "kode_pos",
    ];

    protected $appends = [
        "provinsi",
        "kota",
        "kecamatan",
        "desa"
    ];

    public function getProvinsiAttribute()
    {
        return Indonesia::findProvince($this->provinsi_id);
    }

    public function getKotaAttribute()
    {
        return Indonesia::findCity($this->kota_id);
    }

    public function getKecamatanAttribute()
    {
        return Indonesia::findDistrict($this->kecamatan_id);
    }

    public function getDesaAttribute()
    {
        return Indonesia::findVillage($this->desa_id);
    }

    public function product()
    {
        return $this->hasMany(Product::class, "user_id", "id_user");
    }

    public function user()
    {
        return $this->hasOne(User::class, "id", "id_user");
    }
}
