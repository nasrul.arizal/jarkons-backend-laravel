<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Banner extends Model implements HasMedia
{
    use InteractsWithMedia;
    protected $guarded = [];
    protected $appends = ['gambar'];

    public function getGambarAttribute()
    {
        return $this->getFirstMediaUrl("gambar");
    }

    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection('gambar')
            ->singleFile();
    }
}
