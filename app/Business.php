<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $table = "bussiness";

    protected $fillable = [
        "created_by",
        "business_code",
        "business_entity",
        "desc",
        "status",
    ];

    protected $appends = [
      "creator"
    ];

    protected $hidden = [
      "created_by"
    ];

    public function getCreatorAttribute()
    {
        $user = User::whereId($this->createdBy)->first();
        return ($user) ? $user->name : "Administrator";
    }
}
