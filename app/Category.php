<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Category extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $table = "categories";

    protected $fillable = [
        "parent_id",
        "category",
        "category_desc",
        "created_by",
        "status"
    ];

    protected $appends = [
        "img_web",
        "img_mobile",
        "creator",
        "jenis_produk"
    ];

    public function product()
    {
        return $this->hasMany(Product::class, "id_category", "id");
    }

    public function child()
    {
        return $this->hasMany(Category::class, "parent_id", "id");
    }

    public function jenisProduk()
    {
        return $this->hasOne(ProductType::class, "id", "parent_id");
    }

    public function getImgWebAttribute()
    {
        $media = $this->getMedia("img_web");
        foreach ($media as $item) {
            return $item->getFullUrl();
        }
    }

    public function getJenisProdukAttribute()
    {
        $jenis = ProductType::find($this->parent_id);
        return ($jenis) ? $jenis->product_type : "";
    }

    public function getImgMobileAttribute()
    {
        $media = $this->getMedia("img_mobile");

        foreach ($media as $item) {
            return $item->getFullUrl();
        }
    }

    public function getCreatorAttribute()
    {
        $user = User::find($this->created_by);
        return ($user) ? $user->name : "Administrator";
    }
}
