<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $guarded = [];

    protected $appends = ["stamp", "name", "kepada"];

    public function getStampAttribute()
    {
        return Carbon::parse($this->created_at)->diffForHumans();
    }

    public function user()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }

    public function participant()
    {
        return $this->hasMany(ChatParticipant::class, "room_id", "room_id");
    }

    public function getNameAttribute()
    {
        return $this->user->name;
    }

    public function getKepadaAttribute()
    {
        return $this->participant()->where("user_id", "!=", $this->user_id)->first();
    }

}
