<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatParticipant extends Model
{
    protected $guarded = [];

    protected $appends = ["pengirim"];

    public function user()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }

    public function getPengirimAttribute()
    {
        return $this->user->name;
    }
}
