<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatRoom extends Model
{
    protected $guarded = [];

    public function participant()
    {
        return $this->hasMany(ChatParticipant::class, "room_id", "id");
    }

    public function chat()
    {
        return $this->hasMany(Chat::class, "room_id", "id");
    }
}
