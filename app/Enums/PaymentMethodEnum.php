<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static bni()
 * @method static static bca()
 * @method static static mandiri()
 */
final class PaymentMethodEnum extends Enum
{
    const bni =   "bni";
    const bri =   "bri";
    const bca =   "bca";
    const mandiri = "mandiri";
    const permata = "permata";
    const gopay = "gopay";
    const qris = "qris";
}
