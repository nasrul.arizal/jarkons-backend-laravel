<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static User()
 * @method static static Administrator()
 * @method static static Provider()
 * @method static static Supervisor()
 */
final class RoleUserEnum extends Enum
{
    const User = 0;
    const Administrator = 1;
    const Provider = 2;
    const Supervisor = 3;
}
