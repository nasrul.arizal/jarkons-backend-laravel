<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static active()
 * @method static static inactive()
 * @method static static draft()
 */
final class StatusIklanEnum extends Enum
{
    const active =   0;
    const inactive =   1;
    const draft = 2;
}
