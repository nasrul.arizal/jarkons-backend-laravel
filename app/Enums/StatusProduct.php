<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Draft()
 * @method static static Published()
 * @method static static Unpublished()
 * @method static static Diperiksa()
 * @method static static Disetujui()
 * @method static static Publish()
 * @method static static Unpubish()
 * @method static static Arsip()
 * @method static static Ditolak()
 */
final class StatusProduct extends Enum
{
    const Draft =   "0";
    const Published =   "1";
    const Unpublished = "2";
    const Diperiksa = "3";
    const Disetujui = "4";
    const Publish = "5";
    const Unpubish = "6";
    const Arsip = "7";
    const Ditolak = "8";
}

