<?php

namespace App\Http\Controllers\API;

use App\Enums\StatusProduct;
use App\Http\Controllers\Controller;
use App\Product;
use BenSampo\Enum\Rules\Enum;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Http\Request;
use Validator;

class ApproveProductController extends Controller
{
    public function index(Request $request)
    {
        $product = Product::find($request->id_product);

        $valid = $this->validateProduct($request);

        if ($valid->fails()) {
            return response()->json(['error' => $valid->errors()], 401);
        }

        if (!$product) {
            return response()->json(["error" => "Produk Tidak Ditemukan"], 401);
        } else {
            $product->status = $request->status;
            $product->log()->create([
               "reason" => $request->desc,
               "user_id" => $request->user()->id
            ]);
            $product->save();
            return $product->whereId($product->id)->first();
        }
    }

    private function validateProduct(Request $request)
    {
        return $validator = Validator::make($request->all(), [
            'id_product' => ['required'],
            'status' => ['required', new EnumValue(StatusProduct::class)],
            'desc' => 'required',
        ]);
    }
}
