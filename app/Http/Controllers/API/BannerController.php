<?php

namespace App\Http\Controllers\API;

use App\Banner;
use App\Http\Controllers\Controller;
use App\Services\QueryService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    protected $queryService;

    public function __construct()
    {
        $this->queryService = new QueryService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $bannerQuery = Banner::query();
        $filteredColumn = [
            "judul",
            "description",
            "target_url",
            "status",
            "start",
            "end"
        ];
        return $this->queryService->index($request, $bannerQuery, $filteredColumn);
    }

    public function validation(Request $request)
    {
        $request->validate([
            "judul" => "required",
            "description" => "required",
            "target_url" => "required|url",
            "status" => "required",
            "start" => "required|date",
            "end" => "required|date|after:start"
        ]);

        $data = $request->except("gambar");
        $data["start"] = Carbon::parse($data['start']);
        $data["end"] = Carbon::parse($data['end']);

        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(["gambar" => "required|image"]);
        $data = $this->validation($request);
        $banner = Banner::create($data);
        $banner->addMedia($request->gambar)->toMediaCollection("gambar");
        $banner->gambar = $banner->getFirstMediaUrl("gambar");

        return $this->show($banner->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $banner = Banner::findOrFail($id);
        $banner->gambar = $banner->getFirstMediaUrl("gambar");

        return $banner;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $banner = Banner::findOrFail($id);
        $data = $this->validation($request);
        $banner->update($data);

        if ($request->file("gambar")) {
            $banner->addMedia($request->gambar)->toMediaCollection("gambar");
        }

        $banner->save();

        return $this->show($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = Banner::findOrFail($id);

        $banner->delete();
        return response()->json(["success" => "Berhasil Menghapus Banner", "data" => $banner], 200);
    }

    public function ubahStatus(Request $request, $status = "aktif")
    {
        $banner = Banner::findOrFail($request['id']);
        if ($status == 'aktif') {
            $banner->status = 1;
        } else {
            $banner->status = 0;
        }
        $banner->save();

        return $this->show($request['id']);
    }
}
