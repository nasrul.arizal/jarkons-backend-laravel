<?php

namespace App\Http\Controllers\API;

use App\Business;
use App\Http\Controllers\Controller;
use App\Services\BusinessService;
use Illuminate\Http\Request;

class BusinessController extends Controller
{
    private $businessService;
    public function __construct(BusinessService $businessService)
    {
        $this->businessService = $businessService;
    }

    public function create(Request $request)
    {
        return $this->businessService->create($request);
    }

    public function update(Request $request, $id)
    {
        $business = Business::find($id);
        if (! $business) {
            return response()->json(["error" => "Maaf Badan Usaha Tidak Ditemukan"]);
        } else {
            return $this->businessService->update($request, $business);
        }

    }

    public function index()
    {
        return $this->businessService->list();
    }

    public function select()
    {
        return $this->businessService->select();
    }

    public function get($id)
    {
        $business = Business::find($id);
        if (!$business) {
            return response()->json(["error" => "Badan usaha Tidak Ditemukan"], 401);
        } else {
            return $business;
        }
    }

    public function delete($id)
    {
        $business = Business::find($id);

        if (!$business) {
            return response()->json(["error" => "Badan usaha Tidak Ditemukan"], 401);
        } else {
            $business->delete();
            return response()->json(["success" => "Badan Usaha Berhasil di Hapus"], 200);
        }
    }
}
