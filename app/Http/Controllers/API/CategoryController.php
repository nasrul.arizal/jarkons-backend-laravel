<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Http\Controllers\Controller;
use App\ProductType;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        return $this->categoryService->list();
    }

    public function listSelect()
    {
        return $this->categoryService->listSelect();
    }

    public function create(Request $request)
    {
        return $this->categoryService->create($request);
    }

    public function get($id)
    {
        $category = Category::find($id);
        if (!$category) {
            return response()->json(["error" => "Kategori Tidak Ditemukan"], 401);
        } else {
            return $category;
        }
    }

    public function delete(Request $request, $id)
    {
        $category = Category::find($id);

        if (!$category) {
            return response()->json(["error" => "Kategori Tidak Ditemukan"], 401);
        } else {
            if ($category->product()->count()) {
                return response()->json(["error" => "Kategori Sudah Memiliki Produk
                "], 401);
            } else {
                $category->delete();
                return response()->json(["success" => "Kategori $category->name Berhasil di Hapus"], 200);
            }
        }
    }

    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        if (! $category) {
            return response()->json(["error" => "Kategori Tidak Ditemukan"], 404);
        } else {
            return $this->categoryService->update($request, $category);
        }
    }

    public function menu()
    {
        return Category::all();
    }
}
