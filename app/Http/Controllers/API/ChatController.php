<?php

namespace App\Http\Controllers\API;

use App\Chat;
use App\ChatParticipant;
use App\ChatRoom;
use App\Events\ChatSendEvent;
use App\Http\Controllers\Controller;
use App\Services\ChatService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Broadcast;

class ChatController extends Controller
{
    public $chatService;

    public function __construct(ChatService $chatService)
    {
        $this->chatService = $chatService;
    }

    public function getRoom(Request $request)
    {
        $from = $request->user()->id;
        $kepada = User::findOrFail($request->to);
        $to = $kepada->id;

        $room = $this->chatService->getRoom($from, $to);
        $chating['stamp'] = "kosong";
        $firstChat = $room->chat()->orderBy("created_at", "DESC")->first();
        $room->chatting = $firstChat ?? $chating;

        $participants = ChatParticipant::whereRoomId($room->id)->whereUserId($to)->first();
        $room->tujuan = $participants->pengirim;
        $room->penerima_id = $participants->user->id;
        $room->penerima = $participants->user->avatar;


        return $room;

    }

    public function message(Request $request)
    {
        $from = $request->user()->id;
        $kepada = User::findOrFail($request->to);
        $to = $kepada->id;
        $message = $request->message;

        if ($from && $to && $message) {
            $room = $this->chatService->getRoom($from, $to);

            $pesan = $room->chat()->create([
               "user_id" => $from,
               "message" => $message
            ]);

            event(new ChatSendEvent($pesan));
        } else {
            $pesan = response()->json(["message" => "Pesan Tidak Terkirim"], "500");
        }

        return $pesan;
    }

    public function list(Request $request)
    {
        $from = $request->user()->id;

        $rooms = ChatRoom::whereHas("participant", function ($x) use ($from) {
            return $x->whereUserId($from);
        })->whereHas("chat", function ($y) {
            return $y->orderBy("created_at", "DESC")->take(1);
        })->has("participant")->get();

        foreach ($rooms as $room) {
            $participants = ChatParticipant::whereRoomId($room->id)->where("user_id", "!=", $from)->first();
            $room->tujuan = $participants->pengirim;
            $room->penerima_id = $participants->user->id;
            $room->penerima = $participants->user->avatar;


            $chating['stamp'] = "kosong";
            $firstChat = $room->chat()->orderBy("created_at", "DESC")->first();
            $room->chatting = $firstChat ?? $chating;

        }

        return $rooms;
    }

    public function chatRoom($id, Request $request)
    {
        $room = ChatRoom::findOrFail($id);
        $userId = $request->user()->id;

        $room->chat()->where("user_id", "!=", $userId)->update(["status" => 1]);

        return $room->chat()->orderBy("created_at", "DESC")->paginate(10);
    }

    public function user(Request $request)
    {
        $user = User::all();
        return $user->except($request->user()->id);
    }

    public function count(Request $request)
    {
        $userId = $request->user()->id;
        $sent = Chat::whereUserId($userId)->whereStatus(0)->count();
        $read = Chat::whereUserId($userId)->whereStatus(1)->count();

        $hasil = [
            [
                "status" => "read",
                "count" => $read,
            ],
            [
                "status" => "sent",
                "count" => $sent
            ]
        ];

        return response()->json($hasil, 200);
    }
}
