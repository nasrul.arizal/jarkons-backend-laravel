<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\SyaratDanKetentuanRepository;
use Illuminate\Http\Request;

class CreateFaqController extends Controller
{
    private $repository;
    public function __construct()
    {
        $this->repository = new SyaratDanKetentuanRepository();
    }

    public function __invoke(Request $request)
    {
        return $this->repository->createSyaratKetentuan($request);
    }
}
