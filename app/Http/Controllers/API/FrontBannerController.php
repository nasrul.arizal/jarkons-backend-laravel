<?php

namespace App\Http\Controllers\API;

use App\Banner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FrontBannerController extends Controller
{
    public function __invoke()
    {
        return Banner::whereStatus(1)->get();
    }
}
