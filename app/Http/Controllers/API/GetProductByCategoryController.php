<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GetProductByCategoryController extends Controller
{
    public function index($category, Request $request)
    {
        $categories = Category::where("category", "like", "%" . $category . "%")->first();

        if ($categories) {
            $product = [];

            $productTemp = $categories->product()->with("address")->get();
            foreach ($productTemp as $item) {
                array_push($product, $item);
            }

            $productType = $categories->jenisProduk()->with("child")->first();

            $hasil = [
              "product" => $product,
              "category" => $productType
            ];
            return response()->json($hasil, 200);
        } else {
            return response()->json(["error" =>"Kategori Tidak Ditemukan"], 404);
        }
    }
}
