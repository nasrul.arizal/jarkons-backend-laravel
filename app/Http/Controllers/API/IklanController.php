<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\IklanRepository;
use Illuminate\Http\Request;

class IklanController extends Controller
{
    protected $iklanRepository;
    public function __construct()
    {
        $this->iklanRepository = new IklanRepository();
    }

    public function index(Request $request)
    {
        return $this->iklanRepository->getIklan();
    }

    public function create(Request $request)
    {
        $request->validate([
            "iklan_name" => "required",
            "desc" => "required",
            "target_url" => "required",
            "status" => "required",
            "start" => "required|date",
            "end" => "required|date",
        ]);
        return $this->iklanRepository->createIklan($request);
    }
}
