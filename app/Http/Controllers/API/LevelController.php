<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Level;
use Illuminate\Http\Request;

class LevelController extends Controller
{
    public function create(Request $request)
    {
        $request->validate([
           "level_name" => "required",
           "level_price" => "required",
           "page_access" => "required",
           "status" => "required",
        ]);

        $field = [
          "level_name" => $request->level_name,
          "level_price" => $request->level_price,
          "page_access" => $request->page_access,
          "status" => $request->status,
        ];

        if ($request->status == "true") {
            $field['status'] = 1;
        } elseif ($request->status == "false") {
            $field['status'] = 0;
        }

        return Level::create($field);
    }

    public function list()
    {
        return Level::paginate(10);
    }

    public function updateStatus(Request $request)
    {
        $request->validate([
            "id_level" => "required",
            "status" => "required"
        ]);

        $level = Level::findOrFail($request->id_level);
        if ($request->status == "true" || $request->status == 1) {
            $level->status = false;
        } else {
            $level->status = true;
        }
        $level->save();

        return response()->json(Level::find($level->id), 200);
    }

    public function levelActive()
    {
        return Level::whereStatus(1)->paginate(10);
    }
}
