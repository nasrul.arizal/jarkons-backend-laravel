<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Enums\StatusProduct;
use App\Http\Controllers\Controller;
use App\Services\SearchProductService;
use Illuminate\Http\Request;
use Indonesia;

class LocationController extends Controller
{
    protected $searchProductService;

    public function __construct()
    {
        $this->searchProductService = new SearchProductService();
    }

    public function index($keyword)
    {
        return $this->searchProductService->searchByCity($keyword);
    }

    public function searchProduct($flag, $keyword)
    {
        $city = $this->searchProductService->searchByCity($keyword);

        if ($city->count()) {
            $hasil['lokasi'] = $this->searchProductService->extractKota($city);
            $hasil['product'] = $this->searchProductService->cariProductByCity($hasil['lokasi']['kode_kota']);
            return $hasil;
        } else {
            $hasil['product'] = $this->searchProductService->cariProductByCity();
            return $hasil;
        }
    }

    public function searchProductCategory($category)
    {
        $kategori = Category::whereCategory($category)->first();
        return $kategori->product()->with(["address", "productType"])->has("productType")->has("address")->whereStatus
            (StatusProduct::Publish)->get();
    }
}
