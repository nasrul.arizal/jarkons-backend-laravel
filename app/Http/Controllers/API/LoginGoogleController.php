<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginGoogleController extends Controller
{
    public function __invoke(Request $request)
    {
        $CLIENT_ID = env("GOOGLE_CLIENT_ID");
        $client = new \Google_Client(['client_id' => $CLIENT_ID]);
        $payload = $client->verifyIdToken($request->id_token);

        $user = User::where('google_id', $payload['sub'])->orWhere("email", $payload['email'])->first();
        if ($user) {
            $token =  $user->createToken('nApp')->accessToken;
        } else {
            $newUser = User::create([
                'name' => $payload['name'],
                'email' => $payload['email'],
                'google_id'=> $payload['sub'],
                'password' => $payload['at_hash'],
            ]);
            $token =  $newUser->createToken('nApp')->accessToken;
        }

        $success['token'] =  $token;
        return response()->json(['success' => $success], 200);
    }
}
