<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class NpwpController extends Controller
{
    public function __invoke()
    {
        $client = new Client();

        return $client->request('POST', 'https://ereg.pajak.go.id/ceknpwpbynik', [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8',
                'Host' => 'ereg.pajak.go.id',
                'Origin' => 'https://ereg.pajak.go.id',
                'Referer' => 'https://ereg.pajak.go.id/ceknpwp',
                'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36',
            ]
        ]);
    }
}
