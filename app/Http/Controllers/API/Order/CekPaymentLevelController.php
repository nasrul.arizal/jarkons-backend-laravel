<?php

namespace App\Http\Controllers\API\Order;

use App\Http\Controllers\Controller;
use App\Transaction;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class CekPaymentLevelController extends Controller
{
    public function __invoke($code)
    {
        $transaction = Transaction::whereTransactionCode($code)->first();

        $client = new Client();
        $data = $client->request("get",
            "https://api.sandbox.midtrans.com/v2/$code/status",
            [
                "headers" => [
                    "Accept" => "application/json",
                    'Content-Type' => 'application/json',
                    "Authorization" => "Basic " . base64_encode("SB-Mid-server-SP0qaABfABX_HyQc6_ZiJg1y" . ":")
                ]
            ]);
        $hasil = json_decode($data->getBody());

        if ($hasil->transaction_status == "capture" || $hasil->transaction_status == "settlement") {
            $this->updateOrder($transaction, $hasil);
            return response()->json(["message" => "yes"], 200);
        } else {
            return response()->json(["message" => "no"], 200);
        }
    }

    public function updateOrder($transaction, $hasil)
    {
        $transaction->transaction_status = 1;
        $transaction->fraud_status = 1;
        $transaction->transaction_time = Carbon::parse($hasil->transaction_time);
        $transaction->save();

        $order = $transaction->order;
        $order->status = 1;
        $order->save();

        $user = $order->user;
        $user->level = $order->orderable_id;
        $user->save();
    }
}
