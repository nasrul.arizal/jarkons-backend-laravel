<?php

namespace App\Http\Controllers\API\Order;

use App\Enums\PaymentMethodEnum;
use App\Http\Controllers\Controller;
use App\Level;
use App\Services\OrderLevelService;
use App\User;
use Illuminate\Http\Request;
use BenSampo\Enum\Rules\EnumValue;


class CreteOrderLevelController extends Controller
{
    protected $orderService;

    public function __construct()
    {
        $this->orderService = new OrderLevelService();
    }

    public function __invoke(Request $request)
    {
        $request->validate([
            "level_id" => "required",
            'payment_method' => ['required', new EnumValue(PaymentMethodEnum::class)],

        ]);

        $level = Level::whereId($request->level_id)->whereStatus(1)->first();
        $user = User::findOrFail($request->user()->id);

        return $this->orderService->createOrderLevel($level, $user, $request);
    }
}
