<?php

namespace App\Http\Controllers\API\Order;

use App\Http\Controllers\Controller;
use App\Level;
use App\Order;
use Illuminate\Http\Request;

class GetOrderLevelController extends Controller
{
    public function __invoke(Request $request)
    {
        $user = $request->user();
        $where = [
            "status" => 0,
            "user_id" => $user->id
        ];
        $data = [];

        $order = Order::where($where)->where("orderable_type", "like", "%Level%")->with(["transaction" => function
        ($data) {
            return $data->orderBy("updated_at", "desc");
        },])->orderBy("id", "desc")->first();
        $data['order'] = $order;
        $data['level'] = Level::find($order->orderable_id);
        return $data;
    }
}
