<?php

namespace App\Http\Controllers\API;

use App\Enums\StatusProduct;
use App\Http\Controllers\Controller;
use App\Product;
use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $productService;
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function index(Request $request)
    {
        return $request->user();
    }

    public function draft(Request $request)
    {
        $product = $this->productService->checkMyProduct($request);
        return $product->load("certificate", "portofolio", "reference");
    }

    public function image(Request $request)
    {
        $product = $this->productService->checkMyProduct($request);

        return $this->productService->addImage($product, $request);
    }

    public function create(Request $request)
    {
        return $this->productService->createProduct($request);
    }

    public function certificate(Request $request)
    {
        return $this->productService->createCertificate($request);
    }

    public function portofolio(Request $request, $id = 0)
    {
        return $this->productService->createPortofolio($request, $id);
    }

    public function reference(Request $request)
    {
        return $this->productService->createReference($request);
    }

    public function detail($id)
    {
        return $this->productService->getById($id);
    }

    public function getCertificate($id)
    {
        $product = Product::find($id);
        return ($product) ? $product->certificate
            : response()->json(["error" => "Product Tidak Ditemukan"], 401);
    }

    public function getReference($id)
    {
        $product = Product::find($id);
        return ($product) ? $product->reference
            : response()->json(["error" => "Product Tidak Ditemukan"], 401);
    }

    public function getPortofolio($id)
    {
        return $this->productService->getById($id)->portofolio;
    }

    public function list(Request $request)
    {
        $user = $request->user();

        $product = Product::whereUserId($user->id);

        $perPage = $request->input("perPage") ?? "10";

        return $product->with(["productType", "productCategory"])->paginate($perPage);
    }

    public function delete($id, Request $request)
    {
        $product = Product::find($id);

        if (!$product) {
            return response()->json(['error' => "Produk Tidak Ditemukan"], 401);
        } else {
            if ($request->user()->id != $product->user_id) {
                return response()->json(['error' => "Maaf Ini Bukan Produk Anda"], 401);
            } else {
                $product->delete();
                return response()->json(['success' => "Berhasil Menghapus Produk"], 200);;
            }
        }
    }

    public function diperiksa (Request $request)
    {
        $idProduct = $request->idProduct;
        $product = Product::findOrFail($idProduct);

        $product->status = StatusProduct::Diperiksa();
        $product->save();

        return response()->json("sukses", 200);
    }
}
