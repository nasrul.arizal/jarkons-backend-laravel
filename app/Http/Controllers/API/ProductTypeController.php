<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\ProductType;
use App\Services\ProductTypeService;
use Illuminate\Http\Request;

class ProductTypeController extends Controller
{
    private $productTypeService;

    public function __construct(ProductTypeService $productTypeService)
    {
        $this->productTypeService = $productTypeService;
    }

    public function index(Request $request)
    {
        return $this->productTypeService->list($request);
    }

    public function create(Request $request)
    {
        return $this->productTypeService->create($request);
    }

    public function select()
    {
        return $this->productTypeService->select();
    }

    public function delete($id, Request $request)
    {
        $productType = $this->productTypeService->get($id);

        if ($productType) {
            $productType->delete();
            return response()->json(["success" => "Jenis Product Berhasil di Hapus"], 200);
        } else {
            return response()->json(["error" => "Jenis Product Tidak ditemukan"], 401);
        }
    }

    public function edit($id, Request $request)
    {
        $productType = $this->productTypeService->get($id);

        if ($productType) {
            $this->productTypeService->edit($request, $productType);
            return response()->json(["success" => $productType->find($productType->id)], 200);
        } else {
            return response()->json(["error" => "Jenis Product Tidak ditemukan"], 401);
        }
    }

    public function get($id)
    {
        $productType = ProductType::find($id);

        if ($productType) {
            return response()->json([$productType], 200);
        } else {
            return response()->json(["error" => "Jenis Product Tidak ditemukan"], 401);
        }
    }
}
