<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\ProviderUserService;
use Illuminate\Http\Request;

class ProviderController extends Controller
{
    protected $providerService;

    public function __construct(ProviderUserService $providerService)
    {
        $this->providerService = $providerService;
    }

    public function index(Request $request)
    {
        return $this->providerService->get($request);
    }

    public function create(Request $request)
    {
        return $this->providerService->update($request);
    }

    public function update(Request $request)
    {
        return $this->providerService->update($request);
    }

    public function upload(Request $request)
    {
        return $this->providerService->uploadFoto($request);
    }
}
