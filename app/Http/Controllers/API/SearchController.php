<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Product;
use App\Services\ProductService;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    protected $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function index($keyword)
    {
        return Product::where("name_product", "like", $keyword)->take(10)->get();
    }
}
