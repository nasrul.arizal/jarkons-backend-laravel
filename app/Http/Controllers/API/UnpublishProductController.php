<?php

namespace App\Http\Controllers\API;

use App\Enums\StatusProduct;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;

class UnpublishProductController extends Controller
{
    public function index(Request $request)
    {
        $approve = new ApproveProductController();
        $request['status'] = strval(StatusProduct::Unpubish);
        $request['desc'] = "Unpublish Product";

        return $approve->index($request);
    }

    public function publish(Request $request)
    {
        $approve = new ApproveProductController();

        $product = Product::find($request->id_product);

        if (!$product) {
            return response()->json(["error" => "Produk Tidak Ditemukan"], 401);
        } else {

            if (in_array($product->status, [StatusProduct::Disetujui, strval(StatusProduct::Unpublished), strval(StatusProduct::Unpubish)])) {
                $request['status'] = strval(StatusProduct::Publish);
                $request['desc'] = $product->desc_product;
                return $approve->index($request);
            } else {
                return response()->json(["error" => "Produk Belum Disetujui"], 401);
            }
        }
    }

    public function reject(Request $request)
    {
        $approve = new ApproveProductController();
        $product = Product::find($request->id_product);

        if (!$product) {
            return response()->json(["error" => "Produk Tidak Ditemukan"], 401);
        } else {
            $product->status = StatusProduct::Ditolak;
            $product->log()->create([
                "reason" => $request->desc,
                "user_id" => $request->user()->id
            ]);
            $product->save();
            return $product->whereId($product->id)->first();
        }
    }
}
