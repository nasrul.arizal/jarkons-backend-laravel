<?php

namespace App\Http\Controllers\API;

use App\Address;
use App\ProfileUser;
use App\Services\UserServices;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Validator;

class UserController extends Controller
{

    public $successStatus = 200;
    public $userService;

    public function __construct(UserServices $userService)
    {
        $this->userService = $userService;
    }

    public function login(Request $request)
    {
        return $this->userService->login();
//        return $this->verifyCaptcha($request, $this->userService->login());
    }

    public function beforeRegister(Request $request)
    {
//        return $this->verifyCaptcha($request, $this->register($request));
        return $this->register($request);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $input = $request->all();
        $input['email'] = strtolower($request->email);
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('nApp')->accessToken;
        $success['name'] =  $user->name;

        $this->userService->sendEmailNotif($user);

        return response()->json(['success'=>$success], $this->successStatus);
    }

    public function fail()
    {
        return response()->json(["error" => "Unauthorized"], 401);
    }

    public function details()
    {
        $user = Auth::user();

        if (!$user->profile) {
            ProfileUser::firstOrCreate(["id_user" => $user->id], []);
        }

        if (!$user->address) {
            Address::firstOrCreate(["id_user" => $user->id], [
                "alamat" => "",
                "provinsi_id" => 0,
                "kota_id" => 0,
                "kecamatan_id" => 0,
                "desa_id" => 0,
                "rt" => 0,
                "rw" => 0,
                "kode_pos" => 0
            ]);
        }

        $newUser = $user->whereId($user->id)->with(["profile", "address"])->first();

        return response()->json(['data' => $newUser ], $this->successStatus);
    }

    public function avatar(Request $request)
    {
        if ($request->file()) {
            $validator = $this->userService->validateImage($request);
            if ($validator->fails()) {
                $data = [
                    "error" => "Avatar Type Must Image"
                ];
                return response()->json($data, 401);
            } else {
                $avatar = $this->userService->addAvatar($request);
                return $avatar;
            }
        } else {
            $data = [
                "error" => "Please Enter Image"
            ];
            return response()->json($data, 401);
        }

    }

    public function updateAddress(Request $request)
    {
        return $this->userService->updateAddress($request);
    }

    public function updateProfile(Request $request)
    {
        return $this->userService->updateProfile($request);
    }

    public function getAddress(Request $request)
    {
        return $request->user()->address;
    }

    private function verifyCaptcha(Request $request, $action)
    {
        if ($request->captcha_token) {
            $response = Http::asForm()->post('https://www.google.com/recaptcha/api/siteverify', [
                'response' => $request->captcha_token,
                'secret' => env("CAPTCHA_SECRET_KEY"),
            ]);
            $respon = $response->json();
            if ($respon["success"]) {
                return $action;
            } else {
                return response()->json(['error'=>'Wrong Captcha'], 401);
            }
        } else {
            return response()->json(['error'=>'Please Insert Captcha'], 401);
        }
    }
}
