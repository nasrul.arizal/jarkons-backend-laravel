<?php

namespace App\Http\Controllers\API;

use App\Enums\JenisUserEnum;
use App\Http\Controllers\Controller;
use App\User;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Http\Request;

class UserManager extends Controller
{
    public function list()
    {
        return User::select([
            'id', 'name', 'email', 'password', "jenis_user", "level", "status"
        ])->paginate(10);
    }

    public function get($id, Request $request)
    {
        $user = User::select([
            'id', 'name', 'email', 'password', "jenis_user", "level", "status"
        ])->whereId($id)->first();

        if ($user) {
            return response()->json([
                "success" => $user
            ], 200);
        } else {
            return response()->json([
                "message" => "User Tidak ditemukan",
            ], 401);
        }
    }

    public function edit($id, Request $request)
    {
        $user = User::select([
            'id', 'name', 'email', 'password', "jenis_user", "level", "status"
        ])->whereId($id)->firstOrFail();

        $request->validate([
            "name" => "required",
            "jenis_user" => ["required", new EnumValue(JenisUserEnum::class, false)]
        ]);

        $user->name = $request->name;
        $user->jenis_user = $request->jenis_user;
        $user->save();

        return response()->json([
            "message" => "Berhasil Ubah User",
            "success" => $user
        ], 200);
    }

    public function status(Request $request)
    {
        $user = User::findOrFail($request->id);
        if ($user->status == 0) {
            $user->status = 1;
            $user->save();
            return response()->json([
                "message" => "User Berhasil Di Aktifkan",
                "success" => $user
            ], 200);
        } else {
            $user->status = 0;
            $user->save();
            return response()->json([
                "message" => "User Berhasil Di NON Aktifkan",
                "success" => $user
            ], 200);
        }
    }

    public function assignRole(Request $request)
    {
        $request->validate([
            "user_id" => "required",
            "jenis_user" => "required",
        ]);
        $user = User::findOrFail($request->user_id);
        $user->jenis_user = $request->jenis_user;
        $user->save();

        return response()->json([
            "message" => "Sukses Mengubah Role"
        ], 200);
    }
}
