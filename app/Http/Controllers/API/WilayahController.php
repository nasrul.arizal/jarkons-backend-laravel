<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Indonesia;

class WilayahController extends Controller
{
    public function getProvinsi()
    {
        return Indonesia::allProvinces();
    }

    public function getKota($idProvinsi)
    {
        return Indonesia::findProvince($idProvinsi, ["cities"]);
    }

    public function getKecamatan($idKota)
    {
        return Indonesia::findCity($idKota, ["districts"]);
    }

    public function getDesa($idKecamatan)
    {
        return Indonesia::findDistrict($idKecamatan, ["villages"]);
    }
}
