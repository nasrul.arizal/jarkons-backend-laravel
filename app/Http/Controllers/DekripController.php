<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class DekripController extends Controller
{
    public function __invoke()
    {
        $method = 'aes-256-cbc';
        $key = substr(hash('sha256', "Secret Passphrase", true), 0, 32);
        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
        $encrypted = "U2FsdGVkX19WT28MNI1H/+nQk5y9e6jHvZG6VAbmiwQ=";
        return openssl_encrypt($encrypted, $method, $key, OPENSSL_RAW_DATA, $iv);
    }
}
