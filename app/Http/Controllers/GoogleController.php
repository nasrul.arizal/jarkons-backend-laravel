<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class GoogleController extends Controller
{
    public function index()
    {
        return Socialite::driver("google")->redirect();
    }

    public function callback(Request $request)
    {
        $oauthUser = Socialite::driver('google')->stateless()->user();
        $user = User::where('google_id', $oauthUser->id)->orWhere("email", $oauthUser->email)->first();
        if ($user) {
            $token =  $user->createToken('nApp')->accessToken;
        } else {
            $newUser = User::create([
                'name' => $oauthUser->name,
                'email' => $oauthUser->email,
                'google_id'=> $oauthUser->id,
                'password' => md5($oauthUser->token),
            ]);
            $token =  $newUser->createToken('nApp')->accessToken;
        }

        return redirect(env("FRONTEND_URL") . "/google/" . $token);
    }
}
