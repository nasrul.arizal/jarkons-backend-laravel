<?php

namespace App\Http\Controllers;

use App\Notifications\VerifyEmailNotification;
use App\User;
use Illuminate\Http\Request;
use Notification;

class TestEmailController extends Controller
{
    public function __invoke()
    {
        // TODO: Implement __invoke() method.
        $user = User::find(1);
        $details = [
            'greeting' => 'Hi Nasrul',
            'body' => 'Untuk Melanjutkan Pendaftaran Silahkan Klik Tombol di Bawah ini',
            'thanks' => 'Terima Kasih telah bergabung di Jarkons',
            'actionText' => 'Verify Account',
            'actionURL' => 'https://jarkons.com',
        ];

        Notification::send($user, new VerifyEmailNotification($details));
    }
}
