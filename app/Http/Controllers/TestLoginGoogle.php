<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestLoginGoogle extends Controller
{
    public function __invoke(Request $request)
    {
        $token = $request->id_token;
        $success['token'] =  ($token) ? $token : "kosong";
        return response()->json(['success' => $success], 200);
    }
}
