<?php

namespace App\Http\Controllers;

use App\VerifyEmail;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VerifikasiEmailController extends Controller
{
    public function index($token)
    {
        $verify = VerifyEmail::whereToken($token)->first();

        if ($verify) {
            $verify->user()->update(["email_verified_at" => Carbon::now()]);
            return redirect("https://jarkons.com/#/home");
        } else {
            return redirect("https://jarkons.com/#/");
        }
    }
}
