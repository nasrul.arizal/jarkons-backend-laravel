<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Iklan extends Model implements HasMedia
{
    use InteractsWithMedia;
    protected $guarded = [];
    protected $appends = ["image"];

    public function getImageAttribute()
    {
        return $this->getFirstMediaUrl("image");
    }
}
