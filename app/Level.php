<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $guarded = [];

    public function Order()
    {
        return $this->morphMany(Order::class, 'orderable');
    }
}
