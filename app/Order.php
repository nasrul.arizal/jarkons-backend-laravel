<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];

    public function transaction()
    {
        return $this->hasMany(Transaction::class, "order_id", "id");
    }

    public function orderable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }
}
