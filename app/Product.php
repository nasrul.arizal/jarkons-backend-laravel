<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Product extends Model implements HasMedia
{
    use InteractsWithMedia;
    use SoftDeletes;

    protected $table = "products";

    protected $guarded = ['id'];

    protected $appends = [
        "main_image",
        "image"
    ];

    public function productType()
    {
        return $this->hasOne(ProductType::class, "id", "id_product_type");
    }

    public function productCategory()
    {
        return $this->hasOne(Category::class, "id", "id_category");
    }

    public function getMainImageAttribute()
    {
        return $mainImage = $this->getFirstMediaUrl("main_image");
    }

    public function getImageAttribute()
    {
        $images = $this->getMedia("product_image");
        $hasil = [];

        foreach ($images as $image) {
            array_push($hasil, $image->getFullUrl());
        }

        return $hasil;
    }

    public function certificate()
    {
        return $this->hasMany(ProductCertificate::class, "product_id", "id");
    }

    public function portofolio()
    {
        return $this->hasMany(ProductPortofolio::class, "product_id", "id");
    }

    public function reference()
    {
        return $this->hasMany(ProductReference::class, "product_id", "id");
    }

    public function address()
    {
        return $this->hasOne(Address::class, "id_user", "user_id");
    }

    public function owner()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }

    public function log()
    {
        return $this->hasMany(Log::class, "product_id", "id");
    }
}
