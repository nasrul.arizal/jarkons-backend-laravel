<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCertificate extends Model
{
    protected $table = "product_certificates";

    protected $guarded = ["id"];
}
