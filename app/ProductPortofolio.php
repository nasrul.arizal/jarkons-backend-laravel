<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPortofolio extends Model
{
    protected $table = "product_portofolio";
    protected $guarded = ["id"];
}
