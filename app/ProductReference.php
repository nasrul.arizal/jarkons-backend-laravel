<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductReference extends Model
{
    protected $table = "product_reference";

    protected $guarded = ["id"];
}
