<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class ProductType extends Model implements HasMedia
{
    use InteractsWithMedia;
    protected $table = "product_type";

    protected $fillable = [
        "product_type",
        "level_req",
        "created_by",
        "status",
        "is_jasa",
        "form_tambahan",
    ];

    protected $appends = [
      "creator", "image_mobile", "image_web"
    ];

    protected $hidden = [
      "created_by"
    ];

    public function getCreatorAttribute()
    {
        $user = User::whereId($this->created_by)->first();
        return ($user) ? $user->name : "Administrator";
    }

    public function getImageMobileAttribute()
    {
        return $this->getFirstMediaUrl("image_mobile");
    }

    public function getImageWebAttribute()
    {
        return $this->getFirstMediaUrl("image_web");
    }

    public function business()
    {
        return $this->hasOne(Business::class, "id", "level_req");
    }

    public function child()
    {
        return $this->hasMany(Category::class,"parent_id", "id");
    }

    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection('image_mobile')
            ->singleFile();
        $this
            ->addMediaCollection('image_web')
            ->singleFile();
    }
}
