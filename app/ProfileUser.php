<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileUser extends Model
{
    protected $table = "profile_user";

    protected $fillable = [
        "id_user",
        "ktp",
        "npwp",
        "no_hp",
        "jenis_kelamin",
        "tempat_lahir",
        "tgl_lahir",
        "jenis_user",
        "level",
    ];
}
