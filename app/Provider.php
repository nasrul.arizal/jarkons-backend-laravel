<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Indonesia;

class Provider extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $table = "providers";

    protected $fillable = [
        "user_id",
        "npwp_pemilik",
        "email_pemilik",
        "ktp_pemilik",
        "nama_provider",
        "id_badan_usaha",
        "badan_usaha",
        "no_telp",
        "alamat",
        "provinsi_id",
        "kota_id",
        "kecamatan_id",
        "kelurahan_id",
        "rt",
        "rw",
        "kode_pos",
    ];

    protected $appends = ["foto", "provinsi", "kota", "kecamatan", "desa"];

    public function categories()
    {
        return $this->belongsToMany(Category::class, "provider_category", "provider_id", "category_id");
    }

    public function getFotoAttribute()
    {
        if ($this->hasMedia("foto")) {
            $media = $this->getMedia("foto");
            foreach ($media as $item) {
                return $item->getFullUrl();
            }
        }

        return "";
    }

    public function getProvinsiAttribute()
    {
        return Indonesia::findProvince($this->provinsi_id);
    }

    public function getKotaAttribute()
    {
        return Indonesia::findCity($this->kota_id);
    }

    public function getKecamatanAttribute()
    {
        return Indonesia::findDistrict($this->kecamatan_id);
    }

    public function getDesaAttribute()
    {
        return Indonesia::findVillage($this->kelurahan_id);
    }
}
