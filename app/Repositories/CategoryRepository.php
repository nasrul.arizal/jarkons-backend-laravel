<?php


namespace App\Repositories;


use App\Category;

class CategoryRepository
{
    public function create($field)
    {
        return Category::create($field);
    }

    public function addImgWeb(Category $category, $files)
    {
        if ($category->hasMedia("img_web")) {
            $media = $category->getMedia("img_web");
            foreach ($media as $item) {
                $item->delete();
            }
        }

        $category->addMedia($files)->toMediaCollection("img_web");

        $category->getMedia("img_web");
    }

    public function addImgMobile(Category $category, $files)
    {
        if ($category->hasMedia("img_mobile")) {
            $media = $category->getMedia("img_mobile");

            foreach ($media as $item) {
                $item->delete();
            }
        }

        $category->addMedia($files)->toMediaCollection("img_mobile");
        $category->getMedia("img_mobile");
    }

    public function list()
    {
        return Category::where("status", 1)->paginate(100);
    }

    public function listSelect()
    {
        $category = Category::whereStatus(1)->get();
        $select = [];

        foreach ($category as $item) {
            $itemSelect = [
                "label" => $item->category,
                "product_type" => $item->parent_id,
                "value" => $item->id
            ];
            array_push($select, $itemSelect);
        }

        return $select;
    }
}
