<?php


namespace App\Repositories;


use App\Iklan;
use Illuminate\Http\Request;

class IklanRepository
{
    public function createIklan(Request $request)
    {
        $fillData = [
            "iklan_name" => $request->iklan_name,
            "desc" => $request->desc,
            "target_url" => $request->target_url,
            "status" => $request->status,
            "start" => $request->start,
            "end" => $request->end,
        ];

        $iklan = Iklan::create($fillData);

        if ($request->file("image")) {
            $iklan->addMedia($request->file("image"))->toMediaCollection("image");
            $iklan->getMedia("image");
        }

        if ($iklan) {
            return response()->json([
                "message" => "Berhasil Membuat Iklan",
                "iklan" => $iklan,
            ], 200);
        } else {
            return response()->json([
                "message" => "Ada Error"
            ], 400);
        }
    }

    public function getIklan()
    {
        return Iklan::paginate(10);
    }
}
