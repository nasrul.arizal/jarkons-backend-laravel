<?php


namespace App\Repositories;


use App\Product;
use App\ProductCertificate;

class ProductCertificateRepository
{
    public function addCertificateToProduct($product, $certificateField)
    {
        $this->deleteOldCertificate($product);
        foreach ($certificateField as $item) {
            $field = $this->fillCertificate($item, $product->id);
            ProductCertificate::create($field);
        }
    }

    public function fillCertificate($data, $productId)
    {
        return [
            "product_id" => $productId,
            "certificate_name" => $data->certificate_name,
            "certificate_number" => $data->certificate_number,
            "agency" => $data->agency,
            "issue_date" => $data->issue_date,
            "image_certificate" => $data->image_certificate
        ];
    }

    public function deleteOldCertificate(Product $product)
    {
        if ($product->certificate) {
            foreach ($product->certificate as $item) {
                ProductCertificate::whereId($item->id)->delete();
            }
        }
    }
}
