<?php


namespace App\Repositories;


use App\Product;
use App\ProductPortofolio;

class ProductPortofolioRepository
{
    public function addPortofolioToProduct($product, $certificateField)
    {
        $this->deleteOldCertificate($product);
        foreach ($certificateField as $item) {
            $field = $this->fillCertificate($item, $product->id);
            ProductPortofolio::create($field);
        }
    }

    public function fillCertificate($data, $productId)
    {
        return [
            "product_id" => intval($productId),
            "project_name" => $data->project_name,
            "company_name" => $data->company_name,
            "position" => $data->position,
            "year_start" => $data->year->start,
            "year_end" => $data->year->end,
            "project_place" => $data->project_place
        ];
    }

    public function deleteOldCertificate(Product $product)
    {
        if ($product->portofolio) {
            foreach ($product->portofolio as $item) {
                ProductPortofolio::whereId($item->id)->delete();
            }
        }
    }
}
