<?php


namespace App\Repositories;


use App\Product;
use App\ProductPortofolio;
use App\ProductReference;

class ProductReferenceRepository
{
    public function addReferenceToProduct($product, $certificateField)
    {
        $this->deleteOldCertificate($product);
        foreach ($certificateField as $item) {
            $field = $this->fillCertificate($item, $product->id);
            ProductReference::create($field);
        }
    }

    public function fillCertificate($data, $productId)
    {
        return [
            "product_id" => $productId,
            "company_name" => $data->company_name,
            "file_reference" => $data->file_reference,
        ];
    }

    public function deleteOldCertificate(Product $product)
    {
        if ($product->reference) {
            foreach ($product->reference as $item) {
                ProductReference::whereId($item->id)->delete();
            }
        }
    }
}
