<?php


namespace App\Repositories;


use App\Enums\StatusProduct;
use App\Product;
use Illuminate\Http\Request;

class ProductRespository
{
    private $model;

    public function __construct(Product $product)
    {
        $this->model = $product;
    }

    public function checkProduct($where)
    {
        return Product::firstOrCreate($where, $this->fillProductImage());
    }

    public function updateProduct($product, Request $request)
    {
        $field = $this->fillProduct($request);
        $where = [
            "status" => StatusProduct::Draft
        ];
        $field['user_id'] = $request->user()->id;

        $newProduct = Product::updateOrCreate($where, $field);
        return $newProduct;
    }

    public function fillProductImage()
    {
        return [
          'id_unit_price' => ''
        ];
    }

    public function fillProduct(Request $request)
    {
        return [
            'id_product_type' => $request->id_product_type,
            'id_category' => $request->id_category,
            'name_product' => $request->name_product,
            'desc_product' => $request->desc_product,
            'price' => $request->price,
            'starting_price' => $request->starting_price,
            'final_price' => $request->final_price,
            'price_prone_flag' => $request->price_prone_flag,
            'id_unit_price' => $request->id_unit_price,
            'level' => $request->level,
            'custom_form' => $request->custom_form,
        ];
    }
}
