<?php


namespace App\Repositories;


use App\ProductType;
use Illuminate\Http\Request;

class ProductTypeRepository
{
    public function getAll()
    {
        return ProductType::whereStatus(1)->with("business")->paginate(10);
    }

    public function create($input, Request $request)
    {
        $productType =  ProductType::create($input);

        if ($request->file("image_mobile")) {
            $productType->addMedia($request->file("image_mobile"))->toMediaCollection("image_mobile");
        }

        if ($request->file("image_web")) {
            $productType->addMedia($request->file("image_web"))->toMediaCollection("image_web");
        }

        return $productType;
    }

    public function selectList()
    {
        $productType = ProductType::whereStatus(1)->get();
        $hasil = [];

        foreach ($productType as $item) {
            $data = [
                "label" => $item->product_type,
                "is_jasa" => $item->is_jasa,
                "form" => $item->form_tambahan,
                "value" => $item->id
            ];
            array_push($hasil, $data);
        }

        return $hasil;
    }
}
