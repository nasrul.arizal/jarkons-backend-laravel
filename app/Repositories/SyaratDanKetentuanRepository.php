<?php


namespace App\Repositories;


use App\SyaratDanKetentuan;
use Illuminate\Http\Request;

class SyaratDanKetentuanRepository
{
    public function getLatest()
    {
        return SyaratDanKetentuan::orderBy("id", "asc")->first();
    }

    public function createSyaratKetentuan(Request $request)
    {
        $request->validate([
           "syarat_dan_ketentuan" => "required",
           "category" => "required"
        ]);

        $where = [
            "id" => 1
        ];

        $data = [
            "syarat_dan_ketentuan" => $request->syarat_dan_ketentuan,
            "category" => $request->category,
            "status" => 1
        ];

        return SyaratDanKetentuan::updateOrCreate($where, $data);
    }
}
