<?php


namespace App\Services;

use App\Business;
use Illuminate\Http\Request;
use Validator;

class BusinessService
{
    public function list()
    {
        return Business::paginate(10);
    }

    public function select()
    {
        $business = Business::select("business_entity", "id")->get();
        $hasil = [];

        foreach ($business as $item) {
            $option = [
              "value" => $item->id,
              "label" => $item->business_entity
            ];
            array_push($hasil, $option);
        }

        return $hasil;
    }

    public function create(Request $request)
    {
        $validator = $this->validateRequest($request);
        if ($validator->fails()) {
            return response()->json(['error'=> $validator->errors()], 401);
        }
        $field = $this->fillInput($request);

        if ($request->id) {
            return Business::updateOrCreate(["id" => $request->id], $field);
        } else {
            return Business::create($field);
        }


    }

    private function fillInput(Request $request)
    {
        return [
            "created_by" => $request->user()->id,
            "business_code" => $request->business_code,
            "business_entity" => $request->business_entity,
            "desc" => $request->desc,
        ];
    }

    private function validateRequest(Request $request)
    {
        return Validator::make($request->all(), [
            "business_code" => "required",
            "business_entity" => "required",
            "desc" => "required"
        ]);
    }

    public function update(Request $request, Business $business)
    {
        $valid = $this->validateRequest($request);
        if ($valid->fails()) {
            return response()->json(['error'=> $valid->errors()], 401);
        }

        $field = $this->fillInput($request);
        $business->update($field);

        return $business;
    }
}
