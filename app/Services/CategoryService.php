<?php


namespace App\Services;


use App\Category;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;
use Validator;

class CategoryService
{
    protected  $repo;

    public function __construct(CategoryRepository $repo)
    {
        $this->repo = $repo;
    }

    public function get()
    {

    }

    public function list()
    {
        return $this->repo->list();
    }

    public function listSelect()
    {
        return $this->repo->listSelect();
    }

    public function create(Request $request)
    {
        $valid = $this->validate($request);

        if ($valid->fails()) {
            return response()->json(['error'=> $valid->errors()], 401);
        }

        $field = $this->fillInput($request);

        $category = $this->repo->create($field);

        if ($request->file("img_web")) {
            $this->repo->addImgWeb($category, $request->file("img_web"));
        }

        if ($request->file("img_mobile")) {
            $this->repo->addImgMobile($category, $request->file("img_mobile"));
        }

        $category = Category::find($category->id);

        return $category;
    }

    public function update(Request $request, Category $category)
    {
        $valid = $this->validate($request);
        if ($valid->fails()) {
            return response()->json(['error'=> $valid->errors()], 401);
        }

        $field = $this->fillInput($request);

        if ($request->file("img_web")) {
            $this->repo->addImgWeb($category, $request->file("img_web"));
        }

        if ($request->file("img_mobile")) {
            $this->repo->addImgMobile($category, $request->file("img_mobile"));
        }

        $category->update($field);
        return $category->find($category->id);
    }

    public function fillInput(Request $request)
    {
        return [
            "parent_id" => $request->parent_id,
            "category" => $request->category,
            "category_desc" => $request->category_desc,
            "created_by" => $request->user()->id,
            "status" => 1,
        ];
    }

    public function validate(Request $request)
    {
        return Validator::make($request->all(), [
            "category" => "required",
            "img_web" => "image",
            "img_mobile" => "image"
        ]);
    }
}
