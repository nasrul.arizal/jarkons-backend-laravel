<?php


namespace App\Services;


use App\ChatRoom;

class ChatService
{
    public function getRoom($from, $to)
    {
        $room = ChatRoom::whereType(0)->whereHas("participant", function ($x) use ($from) {
            return $x->whereUserId($from);
        })->whereHas("participant", function ($x) use ($to) {
            return $x->whereUserId($to);
        })->first();

        if (!$room) {
            $room = ChatRoom::create(["type" => 0]);
            $room->participant()->create(["user_id" => $from]);
            $room->participant()->create(["user_id" => $to]);
        }

        return $room;
    }
}
