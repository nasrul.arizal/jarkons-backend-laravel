<?php


namespace App\Services;


use App\Transaction;
use GuzzleHttp\Client;

class MidtransCreateService
{
    public function create(Transaction $transaction)
    {
        $client = new Client();
        $data = $client->request("post", "https://api.sandbox.midtrans.com/v2/charge", [
            "body" => json_encode($this->getBody($transaction)),
            "headers" => [
                "Accept" => "application/json",
                'Content-Type' => 'application/json',
                "Authorization" => "Basic " . base64_encode("SB-Mid-server-SP0qaABfABX_HyQc6_ZiJg1y" . ":")
            ]
        ]);
        $transaction->midtrans = $data->getBody();
        $transaction->save();
        $data =  json_decode($data->getBody());
        return response()->json([
            "midtrans" => $data,
            "transaction" => $transaction,
            "order" => $transaction->order
        ], 200);
    }

    protected function getBody(Transaction $transaction)
    {
        $payment_method = $transaction->payment_method;
        $data = [];
        if ($payment_method == "bni" || $payment_method == "bri" || $payment_method == "bca" || $payment_method == "mandiri") {
            $data["payment_type"] = "bank_transfer";
            $data["bank_transfer"] = [
              "bank" => $transaction->payment_method
            ];
        } elseif ($payment_method == "permata" || $payment_method == "gopay" || $payment_method == "qris") {
            $data["payment_type"] = $transaction->payment_method;
        }

        $data['transaction_details'] = [
            "order_id" => $transaction->transaction_code,
            "gross_amount" => $transaction->total_amount,
        ];
        return $data;
    }
}
