<?php


namespace App\Services;


use App\Level;
use App\Order;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderLevelService
{
    protected $midtrans;
    public function __construct()
    {
        $this->midtrans = new MidtransCreateService();
    }

    public function createOrderLevel($level, User $user, Request $request)
    {
        if (!$level) {
            return response()->json([
                "message" => "Level Tidak Ditemukan"
            ], 400);
        }

        $order = $this->createOrder($level, $user);
        $code = $this->createTransactionCode($order, $level);
        $transaction = $this->createTransaction($order, $code, $request);
        return $this->midtrans->create($transaction);
    }

    protected function createOrder(Level $level, User $user)
    {
        $dataOrder = [
            "user_id" => $user->id,
            "price" => $level->level_price,
            "discount" => 0,
            "total_price" => $level->level_price,
            "orderable_id" => $level->id
        ];
        $where = [
            "status" => 0,
            "user_id" => $user->id,
            "orderable_type" => "App\Level"
        ];

        return $order = Order::updateOrCreate($where, $dataOrder);
    }

    protected function createTransactionCode(Order $order, Level $level)
    {
        $transacionCount = $order->transaction()->count() + 1 . "-" . rand(10000, 99999);
        $transactionCode = "jarkonslvl-" . $level->id . "-" . $order->user->id . "-" . $transacionCount;
        return $transactionCode;
    }

    protected function createTransaction(Order $order, $code, $request)
    {
        $dataTransaction = [
            "transaction_code" => $code,
            "total_amount" => $order->total_price,
            "total_payment" => 0,
            "payment_method" => $request->payment_method,
            "transaction_status" => 0,
            "fraud_status" => 0,
            "transaction_time" => Carbon::now(),
        ];
        return $order->transaction()->create($dataTransaction);
    }
}
