<?php


namespace App\Services;


use App\Enums\StatusProduct;
use App\Product;
use App\ProductPortofolio;
use App\Repositories\ProductCertificateRepository;
use App\Repositories\ProductPortofolioRepository;
use App\Repositories\ProductReferenceRepository;
use App\Repositories\ProductRespository;
use BenSampo\Enum\Rules\EnumKey;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;

class ProductService
{
    private $repo;

    public function __construct(ProductRespository $repo)
    {
        $this->repo = $repo;
    }

    public function checkMyProduct(Request $request)
    {
        $where = [
            "user_id" => $request->user()->id,
            "status" => StatusProduct::Draft
        ];

        return $this->repo->checkProduct($where);
    }

    public function addImage(Product $product, Request $request)
    {
        $validImage = $this->validateImage($request);

        if ($validImage->fails()) {
            return response()->json(['error' => $validImage->errors()], 401);
        }

        if ($request->file("main_image")) {
            $product->clearMediaCollection('main_image');
            if ($request->file("main_image")) {
                $product->addMedia($request->file("main_image"))->toMediaCollection("main_image");
            }
        }
        $tempProductImages = $product->getMedia("product_image");
//        $product->clearMediaCollection('product_image');
        if ($request->file("image")) {
            foreach ($request->file("image") as $item) {
                $product->addMedia($item)->toMediaCollection("product_image");
            }
        }
        if ($request->image_url) {
            foreach ($request->image_url as $imageUrl) {
                $product->addMediaFromUrl($imageUrl)->toMediaCollection("product_image");
            }
        }
        foreach ($tempProductImages as $tempProductImage) {
            $tempProductImage->delete();
        }

        return $product->whereId($product->id)->first();
    }

    public function createProduct(Request $request)
    {
        $validator = $this->validateFill($request);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        if ($request->price_prone_flag == 1) {
            $validPrice = $this->validatePrice($request);
            if ($validPrice->fails()) {
                return response()->json(['error' => $validPrice->errors()], 401);
            }
        } else {
            $request->starting_price = 0;
            $request->final_price = 0;
        }

        $product = $this->checkMyProduct($request);

        return $this->repo->updateProduct($product, $request);
    }

    public function validateImage(Request $request)
    {
        return $validator = Validator::make($request->all(), [
            'image.*' => 'image',
        ]);
    }

    public function validateFill(Request $request)
    {
        return $validator = Validator::make($request->all(), [
            'id_product_type' => 'required',
            'id_category' => 'required',
            'name_product' => 'required',
            'desc_product' => 'required',
            'price' => 'required|numeric',
            'price_prone_flag' => 'required|boolean',
            'id_unit_price' => 'required',
            'level' => 'required',
            'ijazah' => 'image',
        ]);
    }

    public function validatePrice(Request $request)
    {
        return $validator = Validator::make($request->all(), [
            'starting_price' => 'required|numeric',
            'final_price' => 'required|numeric',
        ]);
    }

    public function createCertificate(Request $request)
    {
        $data = json_decode($request->getContent());

        $certificate = $data->certificate;

        $product = $this->checkMyProduct($request);

        $this->addCertificateToProduct($product, $certificate);

        return $product->whereId($product->id)->with("certificate")->first();
    }

    private function addCertificateToProduct(Product $product, $certificate)
    {
        $certificateRepo = new ProductCertificateRepository();

        $certificateRepo->addCertificateToProduct($product, $certificate);
    }

    public function createPortofolio(Request $request, $id = 0)
    {
        $data = json_decode($request->getContent());

        $portofolio = $data->portofolio;

        $product = $this->checkMyProduct($request);

        if ($product) {
            $this->addPortofolioToProduct($product, $portofolio);

            return Product::find($product->id)->with("portofolio")->first();
        } else {
            return response()->json(["message"=> "Produk Tidak ditemukan"],401);
        }

    }

    private function addPortofolioToProduct(Product $product, $portofolio)
    {
        $portofolioRepo = new ProductPortofolioRepository();

        $portofolioRepo->addPortofolioToProduct($product, $portofolio);
    }

    public function createReference(Request $request)
    {
        $data = json_decode($request->getContent());

        $reference = $data->reference;

        $product = $this->checkMyProduct($request);

        $this->addReferenceToProduct($product, $reference);

        return $product->whereId($product->id)->with("reference")->first();
    }

    private function addReferenceToProduct(Product $product, $reference)
    {
        $portofolioRepo = new ProductReferenceRepository();

        $portofolioRepo->addReferenceToProduct($product, $reference);
    }

    public function getByStatus($status, Request $request)
    {
        $status = ucfirst($status);

        $data = [
            'status' => $status
        ];

        $valid = Validator::make($data, [
            'status' => ['required', new EnumKey(StatusProduct::class)]
        ]);

        $status = StatusProduct::getValue($status);

        if ($valid->fails()) {
            return response()->json(['error' => $valid->errors()], 401);
        }
        return Product::whereStatus($status)->paginate(10);
    }

    public function getById($id)
    {
        $product = Product::whereId($id)->with("productType", "productCategory", "address", "owner")->first();

        if (!$product) {
            return response()->json(['error' => "Product Tidak Ditemukan"], 401);
        } else {
            $product->portofolio = $product->portofolio;
            $product->certificate = $product->certificate;
            $product->reference = $product->reference;

            return $product;
        }
    }
}
