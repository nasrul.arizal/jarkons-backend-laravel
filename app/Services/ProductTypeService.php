<?php


namespace App\Services;


use App\ProductType;
use App\Repositories\ProductTypeRepository;
use Illuminate\Http\Request;
use Validator;

class ProductTypeService
{
    private $productTypeRepository;

    public function __construct(ProductTypeRepository $productTypeRepository)
    {
        $this->productTypeRepository = $productTypeRepository;
    }

    public function list(Request $request)
    {
        return $this->productTypeRepository->getAll();
    }

    public function create(Request $request)
    {
        $validator = $this->validateInput($request);

        if ($validator->fails()) {
            return response()->json(['error'=> $validator->errors()], 401);
        }

        $input = $this->fillInput($request);

        return $this->productTypeRepository->create($input, $request);
    }

    private function validateInput(Request $request)
    {
        return Validator::make($request->all(), [
            "product_type" => "required",
            "level_req" => "required|numeric",
        ]);
    }

    private function fillInput(Request $request)
    {
        return [
            "product_type" => $request->product_type,
            "level_req" => $request->level_req,
            "is_jasa" => $request->is_jasa,
            "created_by" => $request->user()->id,
            "form_tambahan" => $request->form_tambahan,
            "status" => 1,
        ];
    }

    public function select()
    {
        return $this->productTypeRepository->selectList();
    }

    public function get($id)
    {
        return ProductType::find($id);
    }

    public function edit($request, ProductType $productType)
    {
        $validator = $this->validateInput($request);

        if ($validator->fails()) {
            return response()->json(['error'=> $validator->errors()], 401);
        }

        $field = $this->fillInput($request);

        if ($request->file("image_mobile")) {
            $productType->addMedia($request->file("image_mobile"))->toMediaCollection("image_mobile");
        }

        if ($request->file("image_web")) {
            $productType->addMedia($request->file("image_web"))->toMediaCollection("image_web");
        }

        $productType->update($field);
    }
}
