<?php


namespace App\Services;


use App\Provider;
use Illuminate\Http\Request;
use Validator;

class ProviderUserService
{
    public function get(Request $request)
    {
        $user = $request->user();

        $this->checkProvider($user);

        return $user;
    }

    public function checkProvider($user)
    {
        Provider::firstOrCreate(["user_id" => $user->id], []);
        $user->provider;
        $user->provider->categories;
    }

    public function update(Request $request)
    {
        $user = $request->user();
        $this->checkProvider($user);

        $this->validate($request);

        $input = $this->fillInput($request);

        Provider::whereUserId($request->user()->id)->update($input);

        $provider = Provider::whereUserId($request->user()->id)->first();

        $category = explode(",", $request->id_kategori);

        if ($request->id_kategori) {
            $provider->categories()->sync($category);
        }

        $user = $user->whereId($user->id)->with("provider")->first();

        $user->provider->categories;

        return $user;
    }

    public function fillInput(Request $request)
    {
        return [
            "user_id" => $request->user()->id,
            "npwp_pemilik" => $request->npwp_pemilik,
            "email_pemilik" => $request->email_pemilik,
            "ktp_pemilik" => $request->ktp_pemilik,
            "nama_provider" => $request->nama_provider,
            "id_badan_usaha" => $request->id_badan_usaha,
            "badan_usaha" => $request->badan_usaha,
            "no_telp" => $request->no_telp,
            "alamat" => $request->alamat,
            "provinsi_id" => $request->provinsi_id,
            "kota_id" => $request->kota_id,
            "kecamatan_id" => $request->kecamatan_id,
            "kelurahan_id" => $request->kelurahan_id,
            "rt" => $request->rt,
            "rw" => $request->rw,
            "kode_pos" => $request->kode_pos,
        ];
    }

    public function validate(Request $request)
    {
        return $request->validate([
            "user_id",
            "npwp_pemilik",
            "email_pemilik",
            "ktp_pemilik",
            "nama_provider",
            "id_badan_usaha",
            "badan_usaha",
            "no_telp",
            "alamat",
            "provinsi_id",
            "kota_id",
            "kecamatan_id",
            "kelurahan_id",
            "rt",
            "rw",
            "kode_pos",
        ]);
    }

    public function uploadFoto(Request $request)
    {
        $user = $request->user();

        $this->checkProvider($user);

        $provider = $user->provider;

        if ($request->file("image")) {
            return $this->processUpload($request, $provider);
        } else {
            return response()->json("Please Insert Image");
        }
    }

    public function processUpload($request, $provider)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'image',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        if ($provider->hasMedia("foto")) {
            $files = $provider->getMedia("foto");
            foreach ($files as $file) {
                $file->delete();
            }
        }

        $provider->addMedia($request->file("image"))->toMediaCollection("foto");

        $data = $provider->whereId($provider->id)->first();

        $data->getMedia("foto");

        return $data;
    }
}
