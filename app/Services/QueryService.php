<?php


namespace App\Services;


use Illuminate\Http\Request;

class QueryService
{
    public function index(Request $request, $query, $filteredColumn = null)
    {
        array_push($filteredColumn, "created_at");
        array_push($filteredColumn, "updated_at");
        if (($request->filter && $request->filter != 'null' && $request->filter != 'undefined') && $filteredColumn) {
            foreach ($filteredColumn as $item) {
                $query->orWhere($item, 'like', '%' . $request->filter . '%');
            }
        }

        if ($request->descending == 'true') {
            $orderRadius = "DESC";
        } else {
            $orderRadius = "ASC";
        }

        if ($request->sortBy && in_array($request->sortBy, $filteredColumn)) {
            $query->orderBy($request->sortBy, $orderRadius);
        }

        if ($request->perPage) {
            return $query->paginate($request->perPage);
        } else {
            return $query->paginate(10);
        }
    }
}
