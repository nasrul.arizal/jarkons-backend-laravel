<?php


namespace App\Services;


use App\Address;
use App\Enums\StatusProduct;
use App\Product;
use Illuminate\Http\Request;
use Indonesia;

class SearchProductService
{
    public function byLocation(Request $request, $keyword)
    {
        $city = $this->searchByCity($keyword);
    }

    public function searchByCity($keyword)
    {
        return Indonesia::search($keyword)->allCities();
    }

    public function extractKota($cities)
    {
        $hasil['jumlah_kota'] = $cities->count();
        $hasil['nama_kota'] = [];
        $hasil['kode_kota'] = [];

        foreach ($cities as $city) {
            array_push($hasil['nama_kota'], $city->name);
            array_push($hasil['kode_kota'], $city->id);
        }

        return $hasil;
    }

    public function cariProductByCity($kodeKota = null)
    {
        if ($kodeKota) {
            $hasil = Product::whereHas("address", function ($q) use ($kodeKota) {
                return $q->whereIn("kota_id", $kodeKota);
            })->has("productType")->with(["address", "productType"])->whereStatus(StatusProduct::Publish)->paginate(100);
        } else {
            $hasil = Product::with(["address", "productType"])->has("productType")->has("address")->whereStatus(StatusProduct::Publish)->paginate(100);
        }

        return $hasil;
    }
}
