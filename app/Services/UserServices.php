<?php


namespace App\Services;


use App\Address;
use App\Notifications\VerifyEmailNotification;
use App\ProfileUser;
use App\User;
use App\VerifyEmail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserServices
{
    public function login()
    {
        if(Auth::attempt(['email' => strtolower(request('email')), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('nApp')->accessToken;
            return response()->json(['success' => $success], 200);
        }
        else{
            return response()->json(['error'=>'Kombinasi Username dan Password Salah'], 401);
        }
    }
    public function addAvatar(Request $request)
    {
        $user = $request->user();

        $user = User::whereEmail($user->email)->first();

        if ($user->hasMedia("avatar")) {
            $this->deleteAvatar($user);
        }

        $user->addMedia($request->file("avatar"))->toMediaCollection("avatar");

        return $user->whereId($user->id)->with("profile", "address")->first();
    }

    public function deleteAvatar($user)
    {
        $media = $user->getMedia("avatar");

        foreach ($media as $item) {
            $item->delete();
        }

        return $media;
    }

    public function validateImage(Request $request)
    {
        return Validator::make($request->all(), [
           "avatar" => "image"
        ]);
    }

    public function updateProfile(Request $request)
    {
        $user = $request->user();
        $where = ["id_user" => $user->id];
        $data = [
            "ktp" => $request->ktp,
            "npwp" => $request->npwp,
            "no_hp" => $request->no_hp,
            "jenis_kelamin" => $request->jenis_kelamin,
            "tempat_lahir" => $request->tempat_lahir,
            "tgl_lahir" => Carbon::make($request->tgl_lahir)->format("Y-m-d"),
        ];

        ProfileUser::where($where)->delete();
        ProfileUser::updateOrCreate($where, $data);

        $user->profile();

        return User::where("id", $user->id)->with("address", "profile")->first();
    }

    public function updateAddress(Request $request)
    {
        $user = $request->user();
        $where = ["id_user" => $user->id];
        $validasi = $this->validateAddress($request);
        if ($validasi->fails()) {
            return response()->json("Please Complete Form", 401);

        } else {
            $data = [
                "alamat" => $request->alamat,
                "provinsi_id" => $request->provinsi_id,
                "kota_id" => $request->kota_id,
                "kecamatan_id" => $request->kecamatan_id,
                "desa_id" => $request->desa_id,
                "rt" => $request->rt,
                "rw" => $request->rw,
                "kode_pos" => $request->kode_pos,
            ];

            Address::where($where)->delete();
            Address::updateOrCreate($where, $data);

            $user->profile();

            return User::where("id", $user->id)->with("address", "profile")->first();
        }

    }

    public function validateAddress(Request $request)
    {
        return Validator::make($request->all(), [
            "alamat" => "required",
            "provinsi_id" => "required",
            "kota_id" => "required",
            "kecamatan_id" => "required",
            "desa_id" => "required",
            "rt" => "required",
            "rw" => "required",
            "kode_pos" => "required",
        ]);
    }

    public function sendEmailNotif($user)
    {
        $token = $this->getToken(30, $user->id) . $user->id;

        VerifyEmail::create([
           "id_user" => $user->id,
           "token" => $token,
           "valid_until" => Carbon::now()->addMinutes(60),
        ]);

        $details = [
            'greeting' => 'Hi ' . $user->name,
            'body' => 'Untuk Melanjutkan Pendaftaran Silahkan Klik Tombol di Bawah ini',
            'thanks' => 'Terima Kasih telah bergabung di Jarkons',
            'actionText' => 'Verify Account',
            'actionURL' => 'https://cdn.jarkons.com/verify-email/' . $token,
        ];

        Notification::send($user, new VerifyEmailNotification($details));
    }

    private function getToken($length, $seed){
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "0123456789";

        mt_srand($seed);      // Call once. Good since $application_id is unique.

        for($i=0;$i<$length;$i++){
            $token .= $codeAlphabet[mt_rand(0,strlen($codeAlphabet)-1)];
        }
        return $token;
    }
}
