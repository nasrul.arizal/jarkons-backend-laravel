<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class User extends Authenticatable implements HasMedia
{
    use Notifiable, HasApiTokens, InteractsWithMedia;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', "jenis_user", "level", "status", "google_id"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'media'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        "avatar"
    ];

    public function profile()
    {
        return $this->hasOne(ProfileUser::class, "id_user", "id");
    }

    public function address()
    {
        return $this->hasOne(Address::class, "id_user", "id");
    }

    public function provider()
    {
        return $this->hasOne(Provider::class, "user_id", "id");
    }

    public function product()
    {
        return $this->hasMany(Product::class, "id_user", "id");
    }

    public function chatParticipant()
    {
        return $this->hasMany(ChatParticipant::class);
    }

    public function getAvatarAttribute()
    {
        $media = $this->getMedia("avatar");

        foreach ($media as $item) {
            return $item->getFullUrl();
        }
    }

    public function order()
    {
        return $this->hasOne(Order::class, "user_id", "id");
    }
}
