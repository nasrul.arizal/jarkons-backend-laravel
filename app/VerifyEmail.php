<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerifyEmail extends Model
{
    protected $table = "verify_email";

    protected $fillable = [
        "id_user",
        "token",
        "valid_until",
    ];

    public function user()
    {
        return $this->hasOne(User::class, "id", "id_user");
    }
}
