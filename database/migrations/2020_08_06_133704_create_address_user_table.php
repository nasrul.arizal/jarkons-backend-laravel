<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address_user', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->bigInteger("id_user");
            $table->integer("provinsi_id");
            $table->integer("kota_id");
            $table->integer("kecamatan_id");
            $table->integer("desa_id");
            $table->string("rt");
            $table->string("rw");
            $table->string("kode_pos");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address_user');
    }
}
