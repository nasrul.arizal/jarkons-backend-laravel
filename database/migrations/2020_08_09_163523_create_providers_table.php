<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->bigInteger("user_id");
            $table->string("npwp_pemilik")->nullable();
            $table->string("email_pemilik")->nullable();
            $table->string("ktp_pemilik")->nullable();
            $table->string("nama_provider")->nullable();
            $table->string("id_badan_usaha")->nullable();
            $table->string("badan_usaha")->nullable();
            $table->string("no_telp")->nullable();
            $table->string("alamat")->nullable();
            $table->integer("provinsi_id")->nullable();
            $table->integer("kota_id")->nullable();
            $table->integer("kecamatan_id")->nullable();
            $table->integer("kelurahan_id")->nullable();
            $table->integer("rt")->nullable();
            $table->integer("rw")->nullable();
            $table->integer("kode_pos")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('providers');
    }
}
