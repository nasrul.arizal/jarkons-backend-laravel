<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->integer("status");
            $table->bigInteger("user_id");
            $table->bigInteger("id_product_type")->default(0);
            $table->bigInteger("id_category")->default(0);
            $table->string("name_product")->default("draft");
            $table->text("desc_product")->nullable();
            $table->float("price")->default(0);
            $table->float("starting_price")->default(0);
            $table->float("final_price")->default(0);
            $table->boolean("price_prone_flag")->default(0);
            $table->string("id_unit_price");
            $table->integer("level")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
