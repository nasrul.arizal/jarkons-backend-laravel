<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_certificates', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->integer("product_id");
            $table->string("certificate_name");
            $table->string("certificate_number");
            $table->string("agency");
            $table->string("issue_date");
            $table->text("image_certificate");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_certificates');
    }
}
