<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductPortofolioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_portofolio', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->unsignedBigInteger("product_id");
            $table->string("project_name");
            $table->string("company_name");
            $table->string("position");
            $table->string("year_start");
            $table->string("year_end");
            $table->string("project_place");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_portofolio');
    }
}
