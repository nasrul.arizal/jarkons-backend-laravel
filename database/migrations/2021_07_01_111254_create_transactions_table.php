<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger("order_id");
            $table->string("transaction_code");
            $table->string("total_amount");
            $table->string("total_payment");
            $table->string("payment_method");
            $table->string("transaction_status");
            $table->string("fraud_status");
            $table->dateTime("transaction_time");
            $table->dateTime("payment_time")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
