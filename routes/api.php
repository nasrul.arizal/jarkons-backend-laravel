<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'API\UserController@login');
Route::post('login-google', 'API\LoginGoogleController');
Route::post('test-login-google', 'TestLoginGoogle');
Route::post('register', 'API\UserController@beforeRegister');
Route::get('dekrip', 'DekripController');
Route::get('npwp', 'API\NpwpController');

Route::get("location/{keyword}", "API\LocationController@index")->name("search.location");
Route::get("product/list/{flag}/{keyword}", "API\LocationController@searchProduct")->name("search.product.location");
Route::get("product/category/{category}", "API\LocationController@searchProductCategory")->name("search.product.category");
Route::get("search/{keyword}", "API\SearchController@index")->name("search.product.api");
Route::get('fail', 'API\UserController@fail')->name("api.user.failed");
Route::get('front/product/detail/{id}', 'API\ProductController@detail')->name("front.detail.product");
Route::get('/category/menu', 'API\CategoryController@menu');
Route::get('product-type/list/select', 'API\ProductTypeController@select');

Route::get("front-banner", "API\FrontBannerController");

Route::group(['middleware' => 'auth:api'], function() {
    Route::post('syarat-dan-ketentuan/create', 'API\CreateFaqController');

    Route::post('details', 'API\UserController@details');
    Route::post('detail', 'API\UserController@details');
    Route::post('add-avatar', 'API\UserController@avatar');

    Route::group(["prefix" => "wilayah"], function() {
        Route::get('/provinsi', 'API\WilayahController@getProvinsi');
        Route::get('/kota/{idProvinsi}', 'API\WilayahController@getKota');
        Route::get('/kecamatan/{idKota}', 'API\WilayahController@getKecamatan');
        Route::get('/desa/{idkecamatan}', 'API\WilayahController@getDesa');
    });

    Route::group(["prefix" => "provider"], function() {
        Route::get('/detail', 'API\ProviderController@index');
        Route::post('/create', 'API\ProviderController@create');
        Route::post('/update', 'API\ProviderController@update');
        Route::post('/image', 'API\ProviderController@upload');
    });

    Route::group(["prefix" => "business"], function() {
        Route::get('/list', 'API\BusinessController@index');
        Route::get('/select', 'API\BusinessController@select');
        Route::post('/create', 'API\BusinessController@create');
        Route::post('/edit/{id}', 'API\BusinessController@update');
        Route::post('/delete/{id}', 'API\BusinessController@delete');
        Route::get('/detail/{id}', 'API\BusinessController@get');
    });

    Route::group(["prefix" => "chat"], function() {
        Route::get('/auth', 'API\ChatController@auth');
    });

    Route::resource("banner", "API\BannerController")->except("edit");
    Route::post("status-banner/{status}", "API\BannerController@ubahStatus");
});
