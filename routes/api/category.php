<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth:api'], function() {

    Route::group(["prefix" => "category"], function() {
        Route::get('/list', 'API\CategoryController@index');
        Route::post('/create', 'API\CategoryController@create');
        Route::post('edit/{id}', 'API\CategoryController@update');
        Route::get('detail/{id}', 'API\CategoryController@get');
        Route::post('delete/{id}', 'API\CategoryController@delete');

    });

});
Route::get('category/list-select', 'API\CategoryController@listSelect');
