<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth:api'], function() {
    Route::group(["prefix" => "chat"], function() {
        Route::post("room", "API\ChatController@getRoom");
        Route::post("message", "API\ChatController@message");
        Route::get("user", "API\ChatController@user");
        Route::post("list", "API\ChatController@list");
        Route::get("notification", "API\ChatController@count");
        Route::post("room/{id}", "API\ChatController@chatRoom");
    });
});
