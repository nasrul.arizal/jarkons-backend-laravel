<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth:api'], function() {
    Route::group(["prefix" => "iklan"], function() {
        Route::get('/', 'API\IklanController@index');
        Route::post('/create', 'API\IklanController@create');
    });
});
