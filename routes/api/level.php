<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth:api'], function() {

    Route::post("/level/create", "API\LevelController@create");
    Route::post("/level/list", "API\LevelController@list");
    Route::get("/level/active", "API\LevelController@levelActive");
    Route::post("/level/update-status", "API\LevelController@updateStatus");

});
