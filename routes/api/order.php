<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth:api'], function() {
    Route::group(["prefix" => "order"], function() {

        Route::post("/level", "API\Order\CreteOrderLevelController");
        Route::get("/level", "API\Order\GetOrderLevelController");
        Route::get("/level/{code}", "API\Order\CekPaymentLevelController");

    });
});
