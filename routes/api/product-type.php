<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth:api'], function() {

    Route::group(["prefix" => "product-type"], function() {
        Route::get('/list', 'API\ProductTypeController@index');
        Route::get('/{id}', 'API\ProductTypeController@get');
        Route::post('/create', 'API\ProductTypeController@create');
        Route::post('/delete/{id}', 'API\ProductTypeController@delete');
        Route::post('/edit/{id}', 'API\ProductTypeController@edit');
    });

});
