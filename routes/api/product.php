<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth:api'], function() {

    Route::group(["prefix" => "product"], function () {
//        Create Product
        Route::post('/draft', 'API\ProductController@draft');
        Route::post('/image', 'API\ProductController@image');
        Route::post('/information/create', 'API\ProductController@create');

        Route::post('/ubahstatus/diperiksa', 'API\ProductController@diperiksa');
        Route::get('/list', 'API\ProductController@list');
        Route::post('/delete/{id}', 'API\ProductController@delete');
        Route::post('/certificate/create', 'API\ProductController@certificate');
        Route::get('/certificate/{id}', 'API\ProductController@getCertificate');
        Route::get('/reference/{id}', 'API\ProductController@getReference');
        Route::post('/portofolio/create', 'API\ProductController@portofolio');
        Route::post('/reference/create', 'API\ProductController@reference');
        Route::get('status/{status}', 'API\ProductByStatusController@index');
        Route::post('approve-produk', 'API\ApproveProductController@index');
        Route::post('unpublish-produk', 'API\UnpublishProductController@index');
        Route::post('publish-produk', 'API\UnpublishProductController@publish');
        Route::post('reject-produk', 'API\UnpublishProductController@reject');
        Route::get('detail/{id}', 'API\ProductController@detail');
        Route::get('detail/portofolio/{id}', 'API\ProductController@getPortofolio');
    });

    Route::post("/level/create", "API\LevelController@index");

});

Route::get("/product/list/{category}", "API\GetProductByCategoryController@index");
