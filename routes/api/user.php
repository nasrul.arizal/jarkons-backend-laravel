<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth:api'], function() {

    Route::group(["prefix" => "users"], function() {
        Route::post('/address/update', 'API\UserController@updateAddress');
        Route::get('/address/details', 'API\UserController@getAddress');
        Route::post('/profile/update', 'API\UserController@updateProfile');
    });

    Route::group(["prefix" => "user"], function() {
        Route::get('/get/{id}', 'API\UserManager@get');
        Route::post('/edit/{id}', 'API\UserManager@edit');
        Route::get('/list', 'API\UserManager@list');
        Route::post('/status', 'API\UserManager@status');
    });
    Route::post('/assign-role', 'API\UserManager@assignRole');
});
